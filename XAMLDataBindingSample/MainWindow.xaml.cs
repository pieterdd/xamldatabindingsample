﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace XAMLBindingTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Simple integer that can be raised by clicking a button in
        /// the GUI. All this weird mumbo jumbo around the integer declaration
        /// ensures that the GUI will be updated when its value changes. If we didn't
        /// use this, the GUI would read the integer once and never update. The latter
        /// would be a one-time binding instead of a one-way binding.
        /// 
        /// This integer is displayed in the GUI using a data binding on a TextBlock.
        /// </summary>
        private int Counter
        {
            get { return (int)GetValue(CounterProperty); }
            set { SetValue(CounterProperty, value); }
        }
        public static readonly DependencyProperty CounterProperty = 
            DependencyProperty.Register("Counter", typeof(int), typeof(MainWindow), new PropertyMetadata(null));

        /// <summary>
        /// A list of strings that is displayed in the GUI using an ItemsControl. See
        /// http://msdn.microsoft.com/en-us/library/system.windows.controls.itemscontrol%28v=VS.95%29.aspx
        /// for more information about this XAML control.
        /// </summary>
        private List<String> ListOfThings
        {
            get { return (List<String>)GetValue(ListOfThingsProperty); }
            set { SetValue(ListOfThingsProperty, value); }
        }
        public static readonly DependencyProperty ListOfThingsProperty =
            DependencyProperty.Register("ListOfThings", typeof(List<String>), typeof(MainWindow), new PropertyMetadata(null));

        public MainWindow()
        {
            Counter = 0;
            ListOfThings = new List<string>();
            ListOfThings.Add("Milk");
            ListOfThings.Add("Cookies");
            ListOfThings.Add("Butter");

            InitializeComponent();
        }

        private void incrementCounter(object sender, RoutedEventArgs e)
        {
            ++Counter;
        }
    }
}
