While researching how XAML and data bindings work, I made this code sample to experiment. Coming from a Java Swing and Qt (C++) background, I usually pull my data into the GUI by interrogating objects in the model and using Observer pattern-like mechanisms to keep the GUI updated. My goal with this experiment was to figure out how this works in C#.

In this example, I do two things.

  1) **Displaying and incrementing an integer-based counter in the GUI.** My MainWindow class holds an integer that is initialized on 0 and can be incremented by calling a function on the MainWindow. The user can click a button to increment the counter and the GUI will be updated accordingly through the data binding.
  
  2) **Displaying a List<string> in the GUI.** MainWindow also holds a List<string> that contains a few items. I've used an [ItemsControl](http://msdn.microsoft.com/en-us/library/system.windows.controls.itemscontrol%28v=VS.95%29.aspx) to generate a TextBlock for every item in the list.